export const FormatCurrency = (amount: number) => {
  try {
    return amount.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
  } catch {
    return amount;
  }
}