import moxios from 'moxios';
import AxiosInstance from '@utils/axios/Axios';

export const SetUpMoxios = (): typeof moxios => {
  beforeEach(() => {
    moxios.install(AxiosInstance);
  });

  afterEach(() => {
    moxios.uninstall(AxiosInstance);
  });

  return moxios;
};