import AxiosInstanceBuilder from './AxiosInstanceBuilder';

const AxiosInstance = AxiosInstanceBuilder({
  envBaseUrl: process.env.PRODUCTS_API_URL as string,
});

export default AxiosInstance;