import axios, { AxiosInstance } from 'axios'

interface IAxiosInstanceBuilder {
  envBaseUrl: string
  timeout?: number
}

/**
 * @param params IAxiosInstanceBuilder
 * @param params.envBaseUrl string
 * @param params.timeout number
 * @returns AxiosInstance
 */
const AxiosInstanceBuilder = ({
  envBaseUrl,
  timeout = 600000,
}: IAxiosInstanceBuilder): AxiosInstance => {

  return axios.create({
    baseURL: envBaseUrl,
    timeout,
  });
};

export default AxiosInstanceBuilder;