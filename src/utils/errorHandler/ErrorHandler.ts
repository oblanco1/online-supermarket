import notification from 'antd/lib/notification';

const ErrorHandler = (error: any) => {
  let message = 'Algo ha salido mal, intentalo nuevamente';

  if (typeof error.message !== 'undefined') {
    message = error.message;
  };

  return notification.error({ message });
};

export default ErrorHandler;