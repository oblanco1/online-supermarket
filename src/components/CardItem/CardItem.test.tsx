import React from 'react';

import { fireEvent, render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

import { Product } from '../../../__mocks__/Product';
import CardItem from './CardItem';

describe('<CardItem />', () => {
  const productMock = Product;

  const onClickSpy = jest.fn();

  const Wrapper = ({ children }: { children: React.ReactNode }) => <MemoryRouter>{children}</MemoryRouter>;

  it('should render component', async () => {

    render(<CardItem {...productMock} onClick={onClickSpy} />, { wrapper: Wrapper });

    await screen.findByText(productMock.name);
    await screen.findByText(productMock.description);
  });

  it('should call add to cart function', async () => {

    render(<CardItem {...productMock} onClick={onClickSpy} />, { wrapper: Wrapper });

    fireEvent.click(await screen.findByText('Añadir al carrito'));

    expect(onClickSpy).toHaveBeenCalledTimes(1);
  });
});