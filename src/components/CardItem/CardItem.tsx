import { FC, MouseEvent } from 'react';
import Button from 'antd/lib/button';
import Card from 'antd/lib/card';
import Typography from 'antd/lib/typography';
import { Link } from 'react-router-dom';

import { IProduct } from '@domain/product/Product';
import { FormatCurrency } from '@utils/number/FormatCurrency';

import styles from './CardItem.module.scss';

const { Meta } = Card;
const { Text } = Typography;

interface ICartItem extends IProduct {
  onClick?: (product: IProduct) => void;
}

const CardItem: FC<ICartItem> = ({
  onClick,
  ...product
}) => {
  const handleClick = (e: MouseEvent) => {
    e.preventDefault();
    onClick?.(product);
  }

  return (
    <Link to={`/product/${product.id}`}>
      <Card
        hoverable
        cover={<img alt={product.name} src={product.image} />}
        className={styles['osm-card']}
        actions={
          [
            <Button onClick={handleClick}>Añadir al carrito</Button>
          ]
        }
      >
        <Meta
          title={product.name}
          description={product.description}
        />
        <Text className={styles['osm-card-price']}>{FormatCurrency(product.price)}</Text>
      </Card>
    </Link>
  )
}

export default CardItem;