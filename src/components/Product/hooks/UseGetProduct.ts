import { useEffect, useState } from 'react';

import { IProduct } from '@domain/product/Product';
import AxiosInstance from '@utils/axios/Axios';
import ErrorHandler from '@utils/errorHandler/ErrorHandler';

export const UseGetProduct = (id: string) => {
  const [product, setProduct] = useState<IProduct>();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    getProduct();
  }, []);

  const getProduct = async () => {
    try {
      setIsLoading(true);
      const request = await AxiosInstance.get(`/products/${id}`);
      setProduct(request.data);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      ErrorHandler(error);
    }
  }

  return { product, isLoading };
};