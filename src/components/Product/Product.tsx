import { useParams } from 'react-router-dom';
import { UseGetProduct } from './hooks/UseGetProduct';
import Card from './Components/Card/Card';

import styles from './Product.module.scss';

const Product = () => {
  const { id } = useParams();

  const { product } = UseGetProduct(id as string);

  return (
    <div className={styles['osm-product']}>
      {product && <Card {...product} />}
    </div>
  )
};

export default Product;