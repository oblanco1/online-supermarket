import { FC, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import Button from 'antd/lib/button';
import notification from 'antd/lib/notification';
import Typography from 'antd/lib/typography';

import InputQuantity from '@components/InputQuantity/InputQuantity';
import { IProduct } from '@domain/product/Product';
import { RootState } from '@redux/reducers';
import { useAppDispatch } from '@redux/store';
import { FormatCurrency } from '@utils/number/FormatCurrency';

import styles from './Card.module.scss';
import { addItem, removeItem, updateItem } from '@redux/actions';

const { Title, Text } = Typography;

const Card: FC<IProduct> = ({
  name,
  image,
  description,
  price,
  amount,
  id,
}) => {
  const { products } = useSelector((state: RootState) => state.cart);

  const dispatch = useAppDispatch();

  const [quantity, setQuantity] = useState(0);
  const [isAdded, setIsAdded] = useState(false);

  useEffect(() => {
    if (products.length > 0) {
      const product = products.find((product) => product.id === id);

      if (product) {
        setQuantity(product.quantity);
        setIsAdded(true)
      }
    }

    return () => undefined;

  }, [products])

  const handleOnAdd = () => {
    setQuantity(prev => prev + 1);
  }

  const handleOnRemove = () => {
    setQuantity(prev => prev - 1);
  }

  const handleCommitAction = () => {
    const item = {
      quantity,
      name,
      image,
      description,
      price,
      amount,
      id,
    }

    if (isAdded) {
      if (quantity === 0) {
        dispatch(removeItem({ id }));
        return notification.success({ message: 'Producto eliminado exitosamente.' });
      }

      dispatch(updateItem(item));
      notification.success({ message: 'Producto actualizado exitosamente.' });
    } else {
      if (quantity === 0) {
        return
      }

      dispatch(addItem(item));
      notification.success({ message: 'Producto agregado exitosamente.' });
    }
  }

  return (
    <div className={styles['osm-product-card']}>
      <div className={styles['osm-product-card-thumbnail']}>
        <img src={image} alt={name} />
      </div>

      <div className={styles['osm-product-card-data']}>
        <Title level={1}>{name}</Title>
        <Text>{description}</Text>
        <Text className={styles['osm-product-card-data-price']}>{FormatCurrency(price)}</Text>

        <div className={styles['osm-product-card-actions']}>
          <InputQuantity
            quantity={quantity}
            onAdd={handleOnAdd}
            onRemove={handleOnRemove}
            isDisabledAdd={quantity === amount}
          />

          <Button data-testid='card-action-commit' onClick={handleCommitAction} size='large'>
            {isAdded && quantity === 0 ? 'Revomer del carrito' : 'Añadir al carrito'}
          </Button>
        </div>
      </div>
    </div>
  )
};

export default Card;