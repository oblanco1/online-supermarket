import React from 'react';
import { fireEvent, screen } from '@testing-library/react';

import { renderWithProviders, setupStore } from '@utils/testing/SetupRedux';
import { Product } from '../../../../../__mocks__/Product';

import Card from './Card';

describe('<Card />', () => {
  const productMock = Product;

  it('should render component', async () => {

    renderWithProviders(
      <Card {...productMock} />,
      {
        preloadedState: {}
      }
    );

    await screen.findByText(productMock.name);
    await screen.findByText(productMock.description);
  });

  it('should add quantity', async () => {
    const q = 4;

    const store = setupStore({
      cart: {
        products: [{ ...productMock, quantity: q }], _persist: {
          version: 1,
          rehydrated: false
        }
      }
    });

    const { rerender } = renderWithProviders(<Card {...productMock} />, { store });

    expect(await screen.getByTestId('input-quantity-label').textContent).toBe('4');

    fireEvent.click(await screen.getByTestId('input-quantity-add'));

    fireEvent.click(await screen.getByTestId('card-action-commit'));

    rerender(<Card {...productMock} />);

    expect(await screen.getByTestId('input-quantity-label').textContent).toBe('5');
  });

  it('should subtract quantity', async () => {
    const q = 4;

    const store = setupStore({
      cart: {
        products: [{ ...productMock, quantity: q }], _persist: {
          version: 1,
          rehydrated: false
        }
      }
    });

    const { rerender } = renderWithProviders(<Card {...productMock} />, { store });

    expect(await screen.getByTestId('input-quantity-label').textContent).toBe('4');

    fireEvent.click(await screen.getByTestId('input-quantity-rm'));

    fireEvent.click(await screen.getByTestId('card-action-commit'));

    rerender(<Card {...productMock} />);

    expect(await screen.getByTestId('input-quantity-label').textContent).toBe('3');
  });

});