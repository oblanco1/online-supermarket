import { FC } from 'react';
import { useNavigate } from 'react-router-dom';

import Button from 'antd/lib/button';
import Card from 'antd/lib/card';
import Col from 'antd/lib/col';
import Divider from 'antd/lib/divider';
import Row from 'antd/lib/row';
import Typography from 'antd/lib/typography';

import { FormatCurrency } from '@utils/number/FormatCurrency';
import { useAppDispatch } from '@redux/store';

import styles from './Summary.module.scss';
import { revomeAll } from '@redux/actions';

const { Title } = Typography;

interface ISummary {
  total: number;
}

const Summary: FC<ISummary> = ({ total }) => {
  const navigate = useNavigate();

  const dispatch = useAppDispatch();

  const handleRemoveAll = () => {
    dispatch(revomeAll());
    navigate('/');
  }

  return (
    <Card>
      <Title level={1}>Resumen</Title>

      <Row>
        <Col className={styles['osm-summary-left-item']} span={12}>
          <Title level={4}>Subtotal</Title>
        </Col>
        <Col className={styles['osm-summary-right-item']} span={12}>
          <Title level={3}>{FormatCurrency(total)}</Title>
        </Col>
      </Row>

      <Divider />

      <Row>
        <Col className={styles['osm-summary-left-item']} span={12}>
          <Title level={4}>Total</Title>
        </Col>
        <Col className={styles['osm-summary-right-item']} span={12}>
          <Title data-testid='summary-total-amount' level={3}>{FormatCurrency(total)}</Title>
        </Col>
      </Row>

      <Divider />

      <Button block>Comprar</Button>
      <Button type="link" block danger onClick={handleRemoveAll}>Limpiar</Button>
    </Card>
  )
}

export default Summary;
