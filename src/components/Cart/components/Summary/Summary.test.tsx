import React from 'react';

import { screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

import Summary from './Summary';
import { renderWithProviders } from '@utils/testing/SetupRedux';

describe('<Summary />', () => {
  it('should render component', async () => {

    renderWithProviders(
      <MemoryRouter>
        <Summary total={15000} />
      </MemoryRouter>, {
      preloadedState: {}
    });

    expect((await screen.findByTestId('summary-total-amount')).textContent).toBe('$15,000.00');
  });
});