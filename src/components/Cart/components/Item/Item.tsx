import { FC } from 'react';

import Button from 'antd/lib/button';
import List from 'antd/lib/list';
import notification from 'antd/lib/notification';

import InputQuantity from '@components/InputQuantity/InputQuantity';
import { IProductToCart } from '@domain/cart/Cart';
import { useAppDispatch } from '@redux/store';
import { removeItem, updateItem } from '@redux/actions';

import styles from './Item.module.scss';
import { FormatCurrency } from '@utils/number/FormatCurrency';

const { Item: ItemComponent } = List;

const Item: FC<IProductToCart> = ({ ...product }) => {
  const dispatch = useAppDispatch();

  const handleOnAdd = () => {
    const q = product.quantity + 1;
    dispatch(updateItem({ ...product, quantity: q }));
  }

  const handleOnRemove = () => {
    const q = product.quantity - 1;

    if (q === 0) {
      dispatch(removeItem({ id: product.id }));
      return notification.success({ message: 'Producto eliminado exitosamente.' });
    }

    dispatch(updateItem({ ...product, quantity: q }));
  }

  const handleRemove = () => {
    dispatch(removeItem({ id: product.id }));
    return notification.success({ message: 'Producto eliminado exitosamente.' });
  }

  return (
    <ItemComponent
      className={styles['osm-cart-item']}
      actions={[
        <InputQuantity
          quantity={product.quantity}
          onAdd={handleOnAdd}
          onRemove={handleOnRemove}
          isDisabledAdd={product.quantity === product.amount}
        />,
        <Button size='small' onClick={handleRemove} danger>Eliminar</Button>
      ]}
      extra={
        <img
          width={272}
          alt={product.name}
          src={product.image}
        />
      }
    >
      <ItemComponent.Meta
        title={product.name}
        description={product.description}
      />
      {FormatCurrency(product.price)}
    </ItemComponent>
  )
}

export default Item;