import React from 'react';
import { fireEvent, screen } from '@testing-library/react';

import { renderWithProviders, setupStore } from '@utils/testing/SetupRedux';
import { Product } from '../../../../../__mocks__/Product';

import Item from './Item';

describe('<Item />', () => {
  const productMock = Product;

  it('should render component', async () => {

    renderWithProviders(
      <Item {...productMock} quantity={5} />,
      {
        preloadedState: {}
      }
    );

    await screen.findByText(productMock.name);
    await screen.findByText(productMock.description);
  });

  it('should add quantity', async () => {
    const q = 5;

    const store = setupStore({
      cart: {
        products: [{ ...productMock, quantity: q }], _persist: {
          version: 1,
          rehydrated: false
        }
      }
    });

    const { rerender } = renderWithProviders(<Item {...store.getState().cart.products[0]} />, { store });

    expect(await screen.getByTestId('input-quantity-label').textContent).toBe('5');

    fireEvent.click(await screen.getByTestId('input-quantity-add'));

    rerender(<Item {...store.getState().cart.products[0]} />);

    expect(await screen.getByTestId('input-quantity-label').textContent).toBe('6');
  });

  it('should subtract quantity', async () => {
    const q = 5;

    const store = setupStore({
      cart: {
        products: [{ ...productMock, quantity: q }], _persist: {
          version: 1,
          rehydrated: false
        }
      }
    });

    const { rerender } = renderWithProviders(<Item {...store.getState().cart.products[0]} />, { store });

    expect(await screen.getByTestId('input-quantity-label').textContent).toBe('5');

    fireEvent.click(await screen.getByTestId('input-quantity-rm'));

    rerender(<Item {...store.getState().cart.products[0]} />);

    expect(await screen.getByTestId('input-quantity-label').textContent).toBe('4');
  });

});