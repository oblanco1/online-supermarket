import { useEffect, useState } from 'react';
import List from 'antd/lib/list';
import Typography from 'antd/lib/typography';

import { useSelector } from 'react-redux';

import { RootState } from '@redux/reducers';

import Item from './components/Item/Item';
import Summary from './components/Summary/Summary';
import styles from './Cart.module.scss';

const { Title } = Typography;

const Cart = () => {
  const { products } = useSelector((state: RootState) => state.cart);

  const [total, setTotal] = useState(0);

  useEffect(() => {

    const sum = products.reduce((accumulator, product) => {
      return accumulator + (product.price * product.quantity);
    }, 0);

    setTotal(sum);

  }, [products])


  return (
    <div className={styles['osm-cart']}>
      <Title level={1}>Carrito de compras</Title>
      <div className={styles['osm-cart-content']}>

        <List
          itemLayout="vertical"
          dataSource={products}
          renderItem={(product) => (
            <Item key={product.id} {...product} />
          )}
        />
        <Summary total={total} />
      </div>
    </div>
  )
};

export default Cart;