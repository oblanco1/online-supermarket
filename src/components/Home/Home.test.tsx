import React from 'react';
import '@testing-library/jest-dom'

import { screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

import { renderWithProviders } from '@utils/testing/SetupRedux';
import { SetUpMoxios } from '@utils/testing/SetupMoxios';

import { Products } from '../../../__mocks__/Products';
import Home from './Home';


describe('<Home />', () => {
  const moxios = SetUpMoxios();

  it('should render component', async () => {
    moxios.stubOnce('GET', '/products', {
      status: 200,
      response: Products,
    });

    renderWithProviders(<MemoryRouter><Home /></MemoryRouter>, {
      preloadedState: {
        cart: {
          products: [],
          _persist: {
            version: 1,
            rehydrated: false
          }
        }
      }
    });

    expect(await screen.findByText(Products[0].name)).toBeInTheDocument();
    expect(await screen.findByText(Products[6].description)).toBeInTheDocument();
  });
});