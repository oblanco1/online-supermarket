import { useSelector } from 'react-redux';
import Divider from 'antd/lib/divider';
import notification from 'antd/lib/notification';
import theme from 'antd/lib/theme';

import { IProduct } from '@domain/product/Product';
import CardItem from '@components/CardItem/CardItem';
import { addItem, updateItem } from '@redux/actions';
import { RootState } from '@redux/reducers';
import { useAppDispatch } from '@redux/store';

import Carousel from './components/Carousel/Carousel';
import GridItems from './components/GridItems/GridItems';
import { UseGetProducts } from './hooks/UseGetProducts';

import styles from './Home.module.scss';

const Home = () => {
  const { token: { colorBgContainer } } = theme.useToken();

  const { products: cartProducts } = useSelector((state: RootState) => state.cart);

  const dispatch = useAppDispatch();

  const { products } = UseGetProducts();

  const handleAdd = (pro: IProduct) => {
    const product = cartProducts.find((p) => p.id === pro.id);

    if (!product) {
      notification.success({ message: 'Producto agregado exitosamente.' });
      return dispatch(addItem({ ...pro, quantity: 1 }));
    }

    const q = product.quantity + 1;

    if (q > pro.amount) {
      return notification.warning({ message: 'Has alcanzado el límite de este producto.' })
    }

    dispatch(updateItem({ ...pro, quantity: q }));
    notification.success({ message: 'Producto agregado exitosamente.' });
  }

  return (
    <>
      <Carousel />
      <div style={{ background: colorBgContainer }} className={styles['osm-home-content']}>
        <Divider className={styles['osm-home-content-title']} orientation="left">Productos</Divider>
        <GridItems>
          {
            products.map((product) => (
              <CardItem key={product.id} {...product} onClick={handleAdd} />
            ))
          }
        </GridItems>
      </div>
    </>
  )
}

export default Home;