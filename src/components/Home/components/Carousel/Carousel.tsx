import CarouselComponent from 'antd/lib/carousel';

import styles from './Carousel.module.scss';

const Carousel = () => {
  return (
    <CarouselComponent autoplay className={styles['osm-home-carousel']}>
      <div className={styles['osm-home-carousel-item']}>
        <img src="https://images.pexels.com/photos/6238307/pexels-photo-6238307.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="" />
      </div>
      <div className={styles['osm-home-carousel-item']}>
        <img src="https://images.pexels.com/photos/6237994/pexels-photo-6237994.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="" />
      </div>
      <div className={styles['osm-home-carousel-item']}>
        <img src="https://images.pexels.com/photos/4046110/pexels-photo-4046110.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="" />
      </div>
    </CarouselComponent>
  )
};

export default Carousel;