import { FC, PropsWithChildren } from 'react';

import styles from './GridItems.module.scss';

const GridItems: FC<PropsWithChildren> = ({ children }) => {
  return (
    <div className={styles['osm-home-grid']}>
      {children}
    </div>
  )
}

export default GridItems;