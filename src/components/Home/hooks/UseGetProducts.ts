import { useEffect, useState } from 'react';

import { IProduct } from '@domain/product/Product';
import AxiosInstance from '@utils/axios/Axios';
import ErrorHandler from '@utils/errorHandler/ErrorHandler';

export const UseGetProducts = () => {
  const [products, setProducts] = useState<IProduct[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    getProducts();
  }, []);

  const getProducts = async () => {
    try {
      setIsLoading(true);
      const request = await AxiosInstance.get('/products');
      setProducts(request.data);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      ErrorHandler(error);
    }
  }

  return { products, isLoading };
};