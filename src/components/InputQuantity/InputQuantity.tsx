import { FC } from 'react';

import Button from 'antd/lib/button';
import Typography from 'antd/lib/typography';
import MinusOutlined from '@ant-design/icons/MinusOutlined';
import PlusOutlined from '@ant-design/icons/PlusOutlined';

import styles from './InputQuantity.module.scss';

const { Text } = Typography;

export interface IInputQuantity {
  quantity: number;
  onAdd: () => void;
  onRemove: () => void;
  isDisabledAdd: boolean;
}

const InputQuantity: FC<IInputQuantity> = ({
  quantity,
  onAdd,
  onRemove,
  isDisabledAdd,
}) => {
  return (
    <div className={styles['osm-quantity']}>
      <div className={styles['osm-quantity-cell']}>
        <Button data-testid='input-quantity-rm' shape="circle" onClick={onRemove} disabled={quantity === 0}>
          <MinusOutlined />
        </Button>
      </div>
      <div className={styles['osm-quantity-cell']}>
        <Text data-testid='input-quantity-label'>{quantity}</Text>
      </div>
      <div className={styles['osm-quantity-cell']}>
        <Button data-testid='input-quantity-add' shape="circle" onClick={onAdd} disabled={isDisabledAdd}>
          <PlusOutlined />
        </Button>
      </div>
    </div>
  );
};

export default InputQuantity;