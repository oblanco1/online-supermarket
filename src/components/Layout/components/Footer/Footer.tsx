import Layout from 'antd/lib/layout';
import Typography from 'antd/lib/typography';

const { Footer: FooterComponent } = Layout;
const { Text } = Typography;

import styles from './Footer.module.scss';

const Footer = () => {
  const currentYear = (new Date()).getFullYear()
  return (
    <FooterComponent className={styles['osm-footer']}>
      <Text>© {currentYear} Online Supermarket. All rights reserved.</Text>
    </FooterComponent>
  )
}

export default Footer;