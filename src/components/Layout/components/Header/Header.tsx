import { Link } from 'react-router-dom';
import Layout from 'antd/lib/layout';
import Col from 'antd/lib/col';
import Row from 'antd/lib/row';
import theme from 'antd/lib/theme';
import Typography from 'antd/lib/typography';
import PlusOutlined from '@ant-design/icons/PlusOutlined';

import SwitchTheme from '../SwitchTheme/SwitchTheme';
import styles from './Header.module.scss';
import Cart from '../Cart/Cart';

const { Header: HeaderComponent } = Layout;
const { Text } = Typography;

const Header = () => {
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  return (
    <HeaderComponent className={styles['osm-header']} style={{ background: colorBgContainer }}>
      <Row className={styles['osm-header-row']}>
        <Col className={styles['osm-header-col-title']} span={12}>
          <Link to="/">
            <Text strong>Online Supermarket</Text>
          </Link>
        </Col>
        <Col className={styles['osm-header-col-options']} span={12}>
          <Link to="/add">
            <Text strong> <PlusOutlined /> Add</Text>
          </Link>
          <Cart />
          <SwitchTheme />
        </Col>
      </Row>

    </HeaderComponent>
  )
}

export default Header;