import { FC } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import Avatar from 'antd/lib/avatar';
import Badge from 'antd/lib/badge';
import ShoppingCartOutlined from '@ant-design/icons/ShoppingCartOutlined';

import { RootState } from '@redux/reducers';

import styles from './Cart.module.scss';

const Cart: FC = () => {
  const { products } = useSelector((state: RootState) => state.cart);
  return (
    <Link to="/cart">
      <Badge className={styles['osm-cart-badge']} count={products.length}>
        <Avatar className={styles['osm-cart-avatar']} icon={<ShoppingCartOutlined />} />
      </Badge>
    </Link>
  )
};

export default Cart;