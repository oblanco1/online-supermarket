import { useSelector } from 'react-redux';
import Switch from 'antd/lib/switch';

import SunIcon from '@components/Icons/theme/Sun';
import MoonIcon from '@components/Icons/theme/Moon';

import { setThemeMode } from '@redux/actions';
import { RootState } from '@redux/reducers';
import { useAppDispatch } from '@redux/store';

import styles from './SwitchTheme.module.scss';

const SwitchTheme = () => {
  const { mode } = useSelector((state: RootState) => state.theme);

  const dispatch = useAppDispatch();

  const handleChange = (checked: boolean) => {
    dispatch(setThemeMode({ mode: checked ? 'light' : 'dark' }))
  }

  return (
    <Switch
      checkedChildren={<SunIcon className={`${styles['osm-theme-icon']} ${styles['osm-theme-icon-light']}`} />}
      unCheckedChildren={<MoonIcon className={`${styles['osm-theme-icon']} ${styles['osm-theme-icon-dark']}`} />}
      checked={mode === 'light'}
      onChange={handleChange}
      className={styles[`osm-theme-switch-${mode}`]}
    />
  )
};

export default SwitchTheme;