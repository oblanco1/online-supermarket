import { Outlet } from 'react-router-dom';
import { useSelector } from 'react-redux';

import ConfigProvider from 'antd/lib/config-provider';
import LayoutComponent from 'antd/lib/layout';
import theme from 'antd/lib/theme'

import { RootState } from '@redux/reducers';

import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import styles from './Layout.module.scss';

const { Content } = LayoutComponent;

const { defaultAlgorithm, darkAlgorithm } = theme;

const Layout = () => {
  const { mode } = useSelector((state: RootState) => state.theme);

  return (
    <ConfigProvider
      theme={{
        algorithm: mode != 'light' ? darkAlgorithm : defaultAlgorithm,
      }}>
      <LayoutComponent className={styles['osm-main']}>
        <Header />
        <Content className={styles['osm-content']}>
          <Outlet />
        </Content>
        <Footer />
      </LayoutComponent>
    </ConfigProvider>
  )
}

export default Layout;