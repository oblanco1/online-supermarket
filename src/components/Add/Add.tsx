import Button from 'antd/lib/button';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import InputNumber from 'antd/lib/input-number';
import Layout from 'antd/lib/layout';
import notification from 'antd/lib/notification';
import Typography from 'antd/lib/typography';

import styles from './Add.module.scss';
import AxiosInstance from '@utils/axios/Axios';

const { Content } = Layout;
const { Item } = Form;
const { Title } = Typography;

const Add = () => {
  const [form] = Form.useForm();

  const handleOnsubmit = async () => {
    try {
      const formData = await form.validateFields();
      const request = await AxiosInstance.post('/products', formData);
      if (request.status === 201) {
        notification.success({ message: 'Producto registrado exitosamente' });
        form.resetFields();
      }
    } catch {
      notification.error({ message: 'No pudimos registrar tu producto, intentalo más tarde' });
    }
  }

  const handleOnCancel = () => {
    form.resetFields();
  }

  return (
    <Content className={styles['osm-add']}>
      <Title level={1}>Agregar producto</Title>
      <Form
        layout="vertical"
        form={form}
      >
        <Item label="Nombre" name="name" rules={[{ required: true }]}>
          <Input />
        </Item>

        <Item label="Descripción" name="description" rules={[{ required: true }]}>
          <Input />
        </Item>

        <Item label="Precio" name="price" rules={[{ required: true }]}>
          <InputNumber min={1} />
        </Item>

        <Item label="Cantidad" name="amount" rules={[{ required: true }]}>
          <InputNumber min={1} />
        </Item>

        <Item label="Dirección de la imagen" name="image" rules={[{ required: true }]}>
          <Input />
        </Item>

        <Item label=" ">
          <Button type="primary" onClick={handleOnsubmit}>
            Submit
          </Button>
          <Button danger type="link" onClick={handleOnCancel}>
            Cancelar
          </Button>
        </Item>
      </Form>
    </Content>
  )
};

export default Add;