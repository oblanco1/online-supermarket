import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { ITheme } from '@domain/theme/Theme'

const initialState: ITheme = {
  mode: 'light'
}

const themeSlice = createSlice({
  name: 'theme',
  initialState,
  reducers: {
    setThemeMode: (state, action: PayloadAction<ITheme>) => {
      state.mode = action.payload.mode
    },
  },
});

export const {
  setThemeMode
} = themeSlice.actions;

export default themeSlice.reducer;