import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { ICart, IProductToCart } from "@domain/cart/Cart";

const initialState: ICart = {
  products: []
}

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addItem: (state, action: PayloadAction<IProductToCart>) => {
      const index = state.products.findIndex(p => p.id === action.payload.id);

      if (index < 0) {
        state.products.unshift(action.payload);
      } else {
        state.products[index] = action.payload;
      }
    },
    removeItem: (state, action: PayloadAction<{ id: string }>) => {
      const products = state.products.filter(p => p.id !== action.payload.id);
      state.products = products;
    },
    updateItem: (state, action: PayloadAction<IProductToCart>) => {
      const index = state.products.findIndex(p => p.id === action.payload.id);
      if (index >= 0) {
        state.products[index].price = action.payload.price;
        state.products[index].amount = action.payload.amount;
        state.products[index].quantity = action.payload.quantity;
      }
    },
    revomeAll: (state) => {
      state.products = [];
    }
  },
});

export const {
  addItem,
  removeItem,
  updateItem,
  revomeAll,
} = cartSlice.actions;

export default cartSlice.reducer;