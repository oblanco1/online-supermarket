import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';

import { ITheme } from '@domain/theme/Theme';
import theme from '@redux/slices/theme/theme';

import { ICart } from '@domain/cart/Cart';
import cart from '@redux/slices/cart/cart';

import { storage } from './storage';

const themePersistConfig = {
  key: 'theme',
  storage: storage,
  blacklist: [],
};

const cartPersistConfig = {
  key: 'cart',
  storage: storage,
  blacklist: [],
};

const rootReducer = combineReducers({
  theme: persistReducer<ITheme>(themePersistConfig, theme),
  cart: persistReducer<ICart>(cartPersistConfig, cart),
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
