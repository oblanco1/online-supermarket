export interface IIcon {
  height?: string | number;
  width?: string | number;
  className?: string;
}