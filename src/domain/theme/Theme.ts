export type ThemeType = 'light' | 'dark';

export interface ITheme {
  mode: ThemeType;
}