import { IProduct } from "@domain/product/Product";

export interface ICart {
  products: IProductToCart[];
}

export interface IProductToCart extends IProduct {
  quantity: number;
}