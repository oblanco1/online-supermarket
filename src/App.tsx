import React, { Suspense, lazy } from 'react';

import { Routes, Route } from 'react-router-dom';

import Spin from 'antd/lib/spin';
import LoadingOutlined from '@ant-design/icons/LoadingOutlined';

// @Redux
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore } from 'redux-persist';

// @Pages
import Home from '@pages/Home';

// @Components
import Layout from '@components/Layout/Layout';
// @Store
import store from '@redux/store';

import '@assets/styles/global-styles.scss';

const Product = lazy(() => import('@pages/Product'));
const Add = lazy(() => import('@pages/Add'));
const Cart = lazy(() => import('@pages/Cart'));

const App = () => {
  const persistor = persistStore(store);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Suspense fallback={
          <Spin indicator={<LoadingOutlined style={{ fontSize: 32 }} spin />} />
        }>
          <Routes>
            <Route path="/" element={<Layout />}>
              <Route index element={<Home />} />
              <Route path="/product/:id" element={<Product />} />
              <Route path="/cart" element={<Cart />} />
              <Route path="/add" element={<Add />} />
            </Route>
          </Routes>
        </Suspense>
      </PersistGate>
    </Provider>
  )
}

export default App;