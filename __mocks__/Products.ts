export const Products = [
  {
    "name": "Leche",
    "image": "https://www.recetasnestlecam.com/sites/default/files/inline-images/vaso-de-leche-productos-lacteos.jpg",
    "price": 75000,
    "amount": 2,
    "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.",
    "id": "1"
  },
  {
    "name": "Cereal",
    "image": "https://www.webstaurantstore.com/images/products/large/729573/2502309.jpg",
    "price": 85300,
    "amount": 67,
    "description": "when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
    "id": "2"
  },
  {
    "name": "Huevos",
    "image": "https://www.sabervivirtv.com/medio/2023/11/03/huevos_fe2ba96b_231103092853_1280x720.jpg",
    "price": 75000,
    "amount": 12,
    "description": "It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    "id": "3"
  },
  {
    "name": "Carne",
    "image": "https://s2.ppllstatics.com/diariovasco/www/multimedia/201906/03/media/cortadas/carne-roja-kS1C-R5uqUSGFlSqj84mn1I9bQuN-624x385@Diario%20Vasco.jpg",
    "price": 85300,
    "amount": 21,
    "description": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.",
    "id": "4"
  },
  {
    "name": "Queso",
    "image": "https://cdn2.cocinadelirante.com/sites/default/files/images/2023/09/como-hacer-queso-con-1-litro-de-leche.jpg",
    "price": 75000,
    "amount": 8,
    "description": "Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia.",
    "id": "5"
  },
  {
    "name": "Arroz",
    "image": "https://www.arrozua.com/wp-content/uploads/2022/10/Disen%CC%83o-sin-ti%CC%81tulo.png",
    "price": 85300,
    "amount": 9,
    "description": "Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics.",
    "id": "6"
  },
  {
    "name": "Frijoles",
    "image": "https://www.saborusa.com/wp-content/uploads/2023/07/sostenibilidad-frijol-1.webp",
    "price": 85300,
    "amount": 32,
    "description": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.",
    "id": "7"
  },
  {
    "name": "Lentejas",
    "image": "https://media.admagazine.com/photos/63d34024e36bb9adbc33002b/4:3/w_2560%2Cc_limit/lentejas-como-fertilizante.jpg",
    "price": 175000,
    "amount": 42,
    "description": "Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).",
    "id": "8"
  },
  {
    "name": "Avena",
    "image": "https://www.elmueble.com/medio/2019/08/25/copos-de-avena_d397fa3d_1200x630.jpg",
    "price": 85300,
    "amount": 56,
    "description": "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form,",
    "id": "9"
  },
  {
    "name": "Pollo",
    "image": "https://carnescastillo.com/wp-content/uploads/2020/05/Pollo-entero-crudo.jpg",
    "price": 375000,
    "amount": 23,
    "description": "The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.",
    "id": "10"
  }
]