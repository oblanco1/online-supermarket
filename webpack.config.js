/* eslint-disable camelcase */
/* eslint-disable import/order */
/* eslint-disable node/no-unpublished-require */
const path = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const dotenv = require('dotenv')
const webpack = require('webpack')

const alias = require('./modules.aliases.ts')
const CopyPlugin = require('copy-webpack-plugin')

const libraryName = 'app.min'

/**
 * @param {any} state State
 * @returns {any} config
 */
module.exports = state => {
  const outputFile = libraryName + '.js'
  const pathEnv = './.env'

  const env = dotenv.config({
    path: pathEnv
  }).parsed

  const envKeys = Object.keys(env).reduce((prev, next) => {
    prev[`process.env.${next}`] = JSON.stringify(env[next])
    return prev
  }, {})


  return {
    output: {
      publicPath: '/',
      path: __dirname + '/dist',
      filename: outputFile,
      umdNamedDefine: true,
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: 'public/index.html',
        filename: __dirname + '/dist/index.html',
        inject: false,
      }),
      new MiniCssExtractPlugin({
        filename: 'assets/css/style.css',
        chunkFilename: '[name].css',
      }),
      new webpack.DefinePlugin(envKeys),
      new CopyPlugin({
        patterns: [
          { from: 'public/*.ico', to: 'favicon.ico' },
        ],
      }),
    ],
    module: {
      rules: [
        {
          test: /\.(ts|tsx|js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: 'ts-loader',
          },
        },
        {
          test: /\.(scss|css|sass)$/,
          use: ['style-loader', 'css-loader', 'sass-loader'],
        },
        {
          test: /\.(eot|svg|ttf|woff|woff2|otf)$/,
          loader: 'file-loader',
          options: {
            name: 'fonts/[name].[ext]',
          },
        },
      ],
    },
    resolve: {
      modules: ['./node_modules', path.resolve(__dirname, 'src')],
      extensions: ['.ts', '.tsx', '.js', '.jsx'],
      alias,
    },
  }
}
