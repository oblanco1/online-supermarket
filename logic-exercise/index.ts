function reverseCharacters(s: string): string {
  const letters = s.match(/[a-zA-Z]/g) || [];
  const reversedLetters = letters.reverse();

  return s.replace(/[a-zA-Z]/g, (char) => reversedLetters.shift() || char);
}