/* eslint-disable no-useless-escape */
/* eslint-disable import/order */
/* eslint-disable node/no-unpublished-require */
const path = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const dotenv = require('dotenv')
const webpack = require('webpack')

const alias = require('./modules.aliases.ts')
const CopyPlugin = require('copy-webpack-plugin')

const libraryName = 'app.min'

/**
 *
 * @param {any} state State
 * @returns {any} config
 */
module.exports = state => {
  const outputFile = libraryName + '.js'

  let pathEnv = './.env.prod'

  switch (state.NODE_ENV) {
    case 'dev':
      pathEnv = './.env.dev'
      break
    case 'st':
      pathEnv = './.env.staging'
      break
    case 'prod':
      pathEnv = './.env.prod'
      break
    default:
      pathEnv = './.env.dev'
      break
  }

  const env = dotenv.config({
    path: pathEnv
  }).parsed

  const envKeys = Object.keys(env).reduce((prev, next) => {
    prev[`process.env.${next}`] = JSON.stringify(env[next])
    return prev
  }, {})

  const plugins = [
    new HtmlWebpackPlugin({
      template: 'public/index.html',
      filename: __dirname + '/dist/index.html',
      inject: false,
    }),
    new webpack.DefinePlugin(envKeys),
    new CopyPlugin({
      patterns: [
        { from: 'public/*.ico', to: 'favicon.ico' },
      ],
    }),
  ]

  const port = envKeys['process.env.PORT'].replace(/\"/g, '')

  return {
    output: {
      publicPath: '/',
      path: __dirname + '/dist',
      filename: outputFile,
      libraryTarget: 'window',
      umdNamedDefine: true,
    },
    devServer: {
      static: {
        directory: path.join(__dirname, 'dist'),
      },
      historyApiFallback: true,
      hot: true,
      port: port || 3000,
      host: '0.0.0.0',
    },
    target: 'web',
    devtool: 'source-map',
    plugins: plugins,
    module: {
      rules: [
        {
          test: /\.(ts|tsx|js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: 'ts-loader',
          },
        },
        {
          test: /\.(scss|css|sass)$/,
          use: ['style-loader', 'css-loader', 'sass-loader'],
        },
        {
          test: /\.(eot|svg|ttf|woff|woff2|otf)$/,
          loader: 'file-loader',
          options: {
            name: 'fonts/[name].[ext]',
          },
        },
      ],
    },
    resolve: {
      modules: [path.resolve('./node_modules'), path.resolve(__dirname, 'src')],
      alias,
      extensions: ['.ts', '.tsx', '.js', '.jsx'],
    },
  }
}
