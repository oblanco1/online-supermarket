module.exports = {
  preset: 'ts-jest',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  transform: {
    '^.+\\.[tj]sx?$': ['ts-jest', { isolatedModules: process.env.CI != null }],
  },
  modulePaths: ['<rootDir>/src'],
  testEnvironment: 'jest-environment-jsdom',
  setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|ico|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/test/mocks/fileMock.js',
    '\\.(css|scss)$': 'identity-obj-proxy',
    '^@assets/(.*)': '<rootDir>/src/assets/$1',
    '^@components/(.*)': '<rootDir>/src/components/$1',
    '^@domain/(.*)': '<rootDir>/src/domain/$1',
    '^@redux/(.*)': '<rootDir>/src/redux/$1',
    '^@utils/(.*)': '<rootDir>/src/utils/$1'
  },
};
