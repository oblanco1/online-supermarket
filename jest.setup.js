// Learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation((query) => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // deprecated
    removeListener: jest.fn(), // deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
});

/**
 * Suppress console errors
 * Axios throws on failed requests
 */
global.console = {
  ...console,
  error: jest.fn(),
  /**
   * Suppress warnings and logs on ci only
   */
  warn: process.env.CI !== undefined ? jest.fn() : console.warn,
  // eslint-disable-next-line no-console
  log: process.env.CI !== undefined ? jest.fn() : console.log,
};
