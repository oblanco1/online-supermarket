const path = require('path')

module.exports = {
  '@assets': path.resolve(__dirname, 'src', 'assets'),
  '@components': path.resolve(__dirname, 'src', 'components'),
  '@domain': path.resolve(__dirname, 'src', 'domain'),
  '@hooks': path.resolve(__dirname, 'src', 'hooks'),
  '@pages': path.resolve(__dirname, 'src', 'pages'),
  '@redux': path.resolve(__dirname, 'src', 'redux'),
  '@utils': path.resolve(__dirname, 'src', 'utils'),
}
